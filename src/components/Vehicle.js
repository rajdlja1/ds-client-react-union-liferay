import React from 'react';

const LOCOMOTIVE = 'lokomotiva';

function renderVehicle(vehicle, setId) {
	let className = '';
	if (vehicle.description.includes(LOCOMOTIVE)) {
		className = 'locomotive';
	} else {
		className = 'vehicle';
	}
	return (
		<span className={className} key={`${setId}-${vehicle.id}-${vehicle.description}`}>
			{vehicle.name}
		</span>
	);
}

export default renderVehicle;
