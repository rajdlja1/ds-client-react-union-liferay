import React from 'react';
import { FormattedMessage, FormattedDate, FormattedTime } from 'react-intl';
import renderVehicle from '../components/Vehicle';
import {
	RideViewModeEnum,
} from '../constants';

/* TODO redirect to another liferay portlet */
function handleEditButton(id) {
	console.log(`Edited Button Clicked: ${id}`);
}

function renderRide(key, ride, rideViewMode) {
	const rideId = ride.id;
	const vehicles = [];

	if (ride.vehicles != null) {
		ride.vehicles.forEach(function(vehicle) {
			vehicles.push(renderVehicle(vehicle, rideId));
		});
	}


	return (
		<tr key={ride.id} className="ride">
			<td />
			{rideViewMode === RideViewModeEnum.TRAIN_OVERVIEW ? (
				<td />
			) : ('')}
			<td>
				{vehicles.length > 0 ? (
					vehicles.map(t => t).reduce((prev, curr) => [prev, ' ', curr])
				) : (
					<FormattedMessage id="label.notHaveConfiguration" />
				)}
			</td>
			<td>
				{ride.driver !== null ? (
					`${ride.driver.firstname} ${ride.driver.lastname}`
				) : (
					<FormattedMessage id="label.notAssigned" />
				)}
			</td>
			<td>{ride.driver !== null ? ride.driver.phoneNunber : ''}</td>
			<td>{ride.routeSection.from.location}</td>
			<td>
				<FormattedDate value={ride.routeSection.departureTime} />{' '}
				<FormattedTime value={ride.routeSection.departureTime} />
			</td>
			<td>{ride.routeSection.to.location}</td>
			<td>
				<FormattedDate value={ride.routeSection.arrivalTime} />{' '}
				<FormattedTime value={ride.routeSection.arrivalTime} />
			</td>
			<td>
				<div className="btn-toolbar" role="toolbar" label="Toolbar with button groups">
					<div className="btn-group mr-2" role="group" label="First group">
						<button className="btn btn-info route-button" onClick={() => handleEditButton(ride.id)}>
							<FormattedMessage id="label.button.editRouteSection" />
						</button>
					</div>
					<div className="btn-group mr-2" role="group" label="First group" />
				</div>
			</td>
		</tr>
	);
}

export default renderRide;
