import loadHeroWidget from '../../widgets/Hero/hero.widget';
import loadContentWidget from '../../widgets/Content/content.widget';
import loadRouteOverviewWidget from '../../widgets/RouteOverview/routeOverview.widget';
import loadTrainOverviewWidget from '../../widgets/TrainOverview/trainOverview.widget';
import loadTrainsetOverviewWidget from '../../widgets/TrainsetOverview/trainsetoverview.widget';
import loadRouteDetailWidget from '../../widgets/RouteDetail/routedetail.widget';
import loadTrainsetDetailWidget from '../../widgets/TrainsetDetail/trainsetdetail.widget';
import loadTrainsetEditorWidget from '../../widgets/TrainsetEditor/editor.widget';

export default [
	{
		path: 'hero',
		getComponent(done) {
			loadHeroWidget(mod => done(mod.default));
		},
	},
	{
		path: 'content',
		getComponent(done) {
			loadContentWidget(mod => done(mod.default));
		},
	},
	{
		path: 'routeOverview',
		getComponent: done => {
			loadRouteOverviewWidget(mod => done(mod.default));
		},
	},
	{
		path: 'trainOverview',
		getComponent: done => {
			loadTrainOverviewWidget(mod => done(mod.default));
		},
	},
	{
		path: 'routeDetail',
		getComponent: done => {
			loadRouteDetailWidget(mod => done(mod.default));
		},
	},
	{
		path: 'trainsetOverview',
		getComponent: done => {
			loadTrainsetOverviewWidget(mod => done(mod.default));
		},
	},
	{
		path: 'trainsetDetail',
		getComponent: done => {
			loadTrainsetDetailWidget(mod => done(mod.default));
		},
	},
	{
		path: 'trainsetEditor',
		getComponent: done => {
			loadTrainsetEditorWidget(mod => done(mod.default));
		},
	},
];
