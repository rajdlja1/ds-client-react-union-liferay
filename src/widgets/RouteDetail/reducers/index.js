import { combineReducers } from 'redux';
import {
	REQUEST_ROUTE,
	RECEIVE_ROUTE,
	SHOW_ROUTE_SECTIONS_CHANGED,
	SELECT_FROM_DATE,
	SELECT_TO_DATE,
	CHECKBOX_CHANGE,
} from '../actions';
import { INVALIDATE_FORM } from '../../TrainsetOverview/actions';

const selectedToDate = (state = new Date(), action) => {
	switch (action.type) {
		case SELECT_TO_DATE:
			return action.dateTime;
		default:
			return state;
	}
};

const selectedFromDate = (state = new Date(), action) => {
	switch (action.type) {
		case SELECT_FROM_DATE:
			return action.dateTime;
		default:
			return state;
	}
};

const todayCheckBox = (state = false, action) => {
	switch (action.type) {
		case CHECKBOX_CHANGE:
			return !state;
		default:
			return state;
	}
};

const routeMan = (
	state = {
		isFetching: false,
		didInvalidate: false,
		item: null,
	},
	action
) => {
	switch (action.type) {
		case INVALIDATE_FORM:
			return {
				...state,
				didInvalidate: true,
			};
		case REQUEST_ROUTE:
			return {
				...state,
				isFetching: true,
				didInvalidate: false,
			};
		case RECEIVE_ROUTE:
			action.trainset.trains.forEach(function(train) {
				train.showRouteSections = false;
			});

			return {
				...state,
				isFetching: false,
				didInvalidate: false,
				item: action.trainset,
				lastUpdated: action.receivedAt,
			};
		default:
			return state;
	}
};

const filteredRoute = (state = {}, action) => {
	switch (action.type) {
		case RECEIVE_ROUTE:
		case REQUEST_ROUTE:
		case INVALIDATE_FORM:
			return {
				...state,
				route: routeMan(state.route, action),
			};
		case SHOW_ROUTE_SECTIONS_CHANGED:
			let i;
			let value = false;
			for (i = 0; i < state.route.item.trains.length; i++) {
				if (state.route.item.trains[i].id === action.train.id) {
					value = state.route.item.trains[i].showRouteSections;
					break;
				}
			}

			return {
				...state,
				route: {
					...state.route,
					item: {
						...state.route.item,
						trains: state.route.item.trains.map(
							(e, j) => (j === i ? { ...e, showRouteSections: !value } : e)
						),
					},
				},
			};
		default:
			return state;
	}
};

const rootReducer = combineReducers({
	filteredRoute,
	selectedToDate,
	selectedFromDate,
	todayCheckBox,
});

export default rootReducer;
