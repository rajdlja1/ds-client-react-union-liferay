import React, { Component } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { saveRideConfiguration } from '../actions';

const LOCOMOTIVE = 'lokomotiva';

function renderVehicle(vehicle) {
	let className = '';
	if (vehicle.description.includes(LOCOMOTIVE)) {
		className = 'draggableLocomotive';
	} else {
		className = 'draggableVehicle';
	}
	return (
		<span className={className} key={`${vehicle.id}-${vehicle.description}`}>
			{vehicle.name}
		</span>
	);
}

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
	const result = Array.from(list);

	if (startIndex > endIndex) {
		endIndex = endIndex + 1;
	}

	const [removed] = result.splice(startIndex, 1);
	result.splice(endIndex, 0, removed);

	return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
	const sourceClone = Array.from(source);
	const destClone = Array.from(destination);
	const [removed] = sourceClone.splice(droppableSource.index, 1);

	destClone.splice(droppableDestination.index, 0, removed);

	const result = {};
	result[droppableSource.droppableId] = sourceClone;
	result[droppableDestination.droppableId] = destClone;

	return result;
};

const grid = 2;

const getItemStyle = (isDragging, draggableStyle) => ({
	// some basic styles to make the items look a bit nicer
	userSelect: 'none',
	padding: grid * 2,
	margin: `0 0 ${grid}px 0`,

	// change background colour if dragging
	background: isDragging ? 'lightgreen' : '#f6f7f7',

	// styles we need to apply on draggables
	...draggableStyle,
});

const getListStyle = isDraggingOver => ({
	background: isDraggingOver ? 'lightblue' : 'white',
	padding: grid,
});

class DropDownEditor extends Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	state = {
		items: Array.from(this.props.vehicles),
		selected: Array.from(this.props.trainsetVehicles),
	};

	/**
	 * A semi-generic way to handle multiple lists. Matches
	 * the IDs of the droppable container to the names of the
	 * source arrays stored in the state.
	 */
	id2List = {
		droppable: 'items',
		droppable2: 'selected',
	};

	getList = id => this.state[this.id2List[id]];

	handleOnDragEnd = result => {
		// const { dispatch } = this.props;
		const { source, destination } = result;

		console.log(source);
		console.log(destination);

		// dropped outside the list
		if (!destination) {
			return;
		}

		if (source.droppableId === destination.droppableId) {
			const items = reorder(this.getList(source.droppableId), source.index, destination.index);

			let state = { items };

			if (source.droppableId === 'droppable2') {
				state = { selected: items };
			}

			this.setState(state);
		} else {
			const result = move(
				this.getList(source.droppableId),
				this.getList(destination.droppableId),
				source,
				destination
			);

			this.setState({
				items: result.droppable,
				selected: result.droppable2,
			});
		}
	};

	handleSubmit(event) {
		event.preventDefault();
		const newConfiguredRide = this.props.ride;
		newConfiguredRide.vehicles = this.state.selected;
		this.props.dispatch(saveRideConfiguration(this.props.ride.id, newConfiguredRide));
	}

	render() {
		return (
			<div>
				<DragDropContext onDragEnd={this.handleOnDragEnd} id="LLL">
					<Droppable droppableId="droppable">
						{(provided, snapshot) => (
							<div
								className="col-sm-6"
								ref={provided.innerRef}
								style={getListStyle(snapshot.isDraggingOver)}
							>
								<h3 className="title">Dostupné vozy</h3>
								{this.state.items.map((item, index) => (
									<Draggable key={item.name} draggableId={item.name} index={index}>
										{(provided, snapshot) => (
											<div
												ref={provided.innerRef}
												{...provided.draggableProps}
												{...provided.dragHandleProps}
												style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
											>
												{renderVehicle(item)}
											</div>
										)}
									</Draggable>
								))}
								{provided.placeholder}
							</div>
						)}
					</Droppable>
					<Droppable droppableId="droppable2">
						{(provided, snapshot) => (
							<div
								className="col-sm-6"
								ref={provided.innerRef}
								style={getListStyle(snapshot.isDraggingOver)}
							>
								<h3 className="title">Vozy zapojené v soupravě</h3>
								{this.state.selected.map((item, index) => (
									<Draggable key={item.name} draggableId={item.name} index={index}>
										{(provided, snapshot) => (
											<div
												ref={provided.innerRef}
												{...provided.draggableProps}
												{...provided.dragHandleProps}
												style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
											>
												{renderVehicle(item)}
											</div>
										)}
									</Draggable>
								))}
								{provided.placeholder}
							</div>
						)}
					</Droppable>
					<form onSubmit={this.handleSubmit}>
						<div className="form-group">
							<input type="submit" value="Ulož" className="btn btn-primary" />
						</div>
					</form>
				</DragDropContext>
			</div>
		);
	}
}

DropDownEditor.propTypes = {
	dispatch: PropTypes.func.isRequired,
	ride: PropTypes.shape({
		id: PropTypes.number,
	}),
	trainsetVehicles: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string,
			description: PropTypes.string,
		})
	),
	vehicles: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string,
			description: PropTypes.string,
		})
	),
};

const mapStateToProps = () => {
	return {};
};

export default connect(mapStateToProps)(DropDownEditor);
