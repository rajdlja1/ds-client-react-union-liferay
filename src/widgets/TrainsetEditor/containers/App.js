import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { FormattedMessage, FormattedTime, FormattedDate } from 'react-intl';

import { fetchAvailableVehicles, fetchRide } from '../actions';
import getFormattedDate from '../../../components/DateFormatter';
import DropDownEditor from '../components/DropDownEditor';

class App extends Component {
	static propTypes = {
		availableVehicles: PropTypes.shape(),
		dispatch: PropTypes.func.isRequired,
		isFetchingAV: PropTypes.bool,
		isFetchingT: PropTypes.bool,
		ride: PropTypes.shape(),
	};

	componentDidMount() {
		const { dispatch } = this.props;

		const date = moment('2018-04-01 9:00', 'YYYY-MM-DD HH:mm').toDate();
		const id = 1;
		dispatch(fetchAvailableVehicles(getFormattedDate(date), getFormattedDate(date), id));
		dispatch(fetchRide(1));
	}

	render() {
		console.log(this.props.ride);
		return (
			<div className="container">
				<div className="jumbotron">
					<div className="row">
						<div className="col-sm-10">
							<h1>
								<FormattedMessage id="label.editTrainsetWindow" />
							</h1>
						</div>
					</div>
				</div>
				{this.props.isFetchingAV || this.props.isFetchingT ? (
					<h2>
						<FormattedMessage id="label.loading" />
					</h2>
				) : (
					<div style={{ opacity: this.props.isFetchingAV || this.props.isFetchingT ? 0.5 : 1 }}>
						<div>
							<h2>
								Úsek {this.props.ride.routeSection.from.location} -{' '}
								{this.props.ride.routeSection.to.location}
							</h2>
							<h3>
								<FormattedMessage id="label.timeOfDeparture" />{' '}
								<FormattedDate value={this.props.ride.routeSection.departureTime} />{' '}
								<FormattedTime value={this.props.ride.routeSection.departureTime} />
							</h3>
							<h3>
								<FormattedMessage id="label.timeOfArrival" />
								<FormattedDate value={this.props.ride.routeSection.arrivalTime} />{' '}
								<FormattedTime value={this.props.ride.routeSection.arrivalTime} />
							</h3>
						</div>
						<div className="DropDownEditor">
							<DropDownEditor
								vehicles={this.props.availableVehicles.vehicles}
								trainsetVehicles={this.props.ride.vehicles}
								ride={this.props.ride}
							/>
						</div>
					</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { filteredAvailableVehicles } = state;
	const { filteredRide } = state;
	const {
		isFetching: isFetchingAV,
		item: availableVehicles,
	} = filteredAvailableVehicles.availableVehicles || {
		isFetching: true,
		item: null,
	};

	const { isFetching: isFetchingT, item: ride } = filteredRide.ride || {
		isFetching: true,
		item: null,
	};

	return {
		availableVehicles,
		isFetchingAV,
		ride,
		isFetchingT,
	};
};

export default connect(mapStateToProps)(App);
