/* TODO Change hardcoded URLs */

export const REQUEST_TRAINSET = 'REQUEST_TRAINSET';
export const RECEIVE_TRAINSET = 'RECEIVE_TRAINSET';
export const SHOW_ROUTE_SECTIONS_CHANGED = 'SHOW_ROUTE_SECTIONS_CHANGED';
export const SELECT_FROM_DATE = 'SELECT_FROM_DATE';
export const SELECT_TO_DATE = 'SELECT_TO_DATE';
export const INVALIDATE_FORM = 'INVALIDATE_FORM';
export const CHECKBOX_CHANGE = 'CHECKBOX_CHANGE';

export const showRouteSectionsChanged = train => ({
	type: SHOW_ROUTE_SECTIONS_CHANGED,
	train,
});

export const selectFromDate = dateTime => ({
	type: SELECT_FROM_DATE,
	dateTime,
});

export const checkBoxChanged = () => ({
	type: CHECKBOX_CHANGE,
});

export const selectToDate = dateTime => ({
	type: SELECT_TO_DATE,
	dateTime,
});

export const fetchTrainset = () => {
	return dispatch => {
		dispatch(requestTrainset());
		return fetch('http://localhost:8080/soupravy/1')
			.then(response => response.json())
			.then(json => dispatch(receiveTrainset(json)));
	};
};

export const requestTrainset = () => ({
	type: REQUEST_TRAINSET,
});

export const receiveTrainset = json => ({
	type: RECEIVE_TRAINSET,
	trainset: json,
	receivedAt: Date.now(),
});

export const invalidateForm = () => ({
	type: INVALIDATE_FORM,
});

const shouldReloadTrainset = state => {
	const posts = state.filteredPosts;
	if (!posts) {
		return true;
	}
	if (posts.isFetching) {
		return false;
	}
	return posts.didInvalidate;
};

export const reloadTrainsetIfNeeded = (since, until, time) => (dispatch, getState) => {
	if (shouldReloadTrainset(getState())) {
		return dispatch(reloadItems(since, until, time));
	}
};

const reloadItems = (since, until, time) => dispatch => {
	dispatch(requestTrainset());
	return fetch(`http://localhost:8080/soupravy/1/hledej?od=${since}&do=${until}&cas=${time}`)
		.then(response => response.json())
		.then(json => dispatch(receiveTrainset(json)));
};
