import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import renderTrainTable from './TrainTable';
import { FormattedMessage } from 'react-intl';
import SearchForm from './SearchForm';

const React = require('react');

function renderTrainset(trainset) {
	return (
		<div className="container">
			<div className="jumbotron">
				<div className="row">
					<div className="col-sm-8">
						<h1>
							<FormattedMessage id="label.trainSet.detail" /> {trainset.name}
						</h1>
					</div>
					<div className="col-sm-4">
						<h2 />
					</div>
				</div>
			</div>
			<MuiThemeProvider>
				<SearchForm />
			</MuiThemeProvider>

			<h2>Jmeno soupravy: {trainset.name}</h2>
			{renderTrainTable(trainset)}
		</div>
	);
}

export default renderTrainset;
