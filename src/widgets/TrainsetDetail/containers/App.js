import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { fetchTrainset } from '../actions';
import renderTrainset from '../components/TrainsetDetail';

class App extends Component {
	static propTypes = {
		dispatch: PropTypes.func.isRequired,
		isFetching: PropTypes.bool.isRequired,
		trainset: PropTypes.shape(),
	};

	componentDidMount() {
		const { dispatch } = this.props;
		dispatch(fetchTrainset());
	}

	render() {
		const { trainset, isFetching } = this.props;
		return (
			<div className="container">
				{isFetching ? (
					<h2>
						<FormattedMessage id="label.loading" />
					</h2>
				) : (
					<div style={{ opacity: isFetching ? 0.5 : 1 }}>{renderTrainset(trainset)}</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { filteredTrainset } = state;
	const { isFetching, lastUpdated, item: trainset } = filteredTrainset.trainset || {
		isFetching: true,
		item: null,
	};

	return {
		trainset,
		isFetching,
		lastUpdated,
	};
};

export default connect(mapStateToProps)(App);
