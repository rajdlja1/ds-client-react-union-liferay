import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { fetchTrainsetsIfNeeded } from '../actions';
import TrainsetTable from '../components/TrainsetTable';
import SearchForm from '../components/SearchForm';

class App extends Component {
	static propTypes = {
		dispatch: PropTypes.func.isRequired,
		isFetching: PropTypes.bool.isRequired,
		trainsets: PropTypes.array.isRequired,
	};

	componentDidMount() {
		const { dispatch } = this.props;
		dispatch(fetchTrainsetsIfNeeded());
	}

	render() {
		const { trainsets, isFetching } = this.props;
		const isEmpty = trainsets.length === 0;
		return (
			<div className="container">
				<div className="jumbotron">
					<div className="row">
						<div className="col-sm-10">
							<h1>
								<FormattedMessage id="label.dashboard" />
							</h1>
						</div>
						<div className="col-sm-2">
							<h2>
								<FormattedMessage id="label.trainSets" />
							</h2>
						</div>
					</div>
				</div>

				<MuiThemeProvider>
					<SearchForm />
				</MuiThemeProvider>
				{isEmpty ? (
					isFetching ? (
						<h2>
							<FormattedMessage id="label.loading" />.
						</h2>
					) : (
						<h2>
							<FormattedMessage id="label.noRecordsFound" />.
						</h2>
					)
				) : (
					<div style={{ opacity: isFetching ? 0.5 : 1 }}>
						<TrainsetTable trainsets={trainsets} />
					</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { filteredTrainsets } = state;
	const { isFetching, items: trainsets } = filteredTrainsets.trainsets || {
		isFetching: true,
		items: [],
	};

	return {
		trainsets,
		isFetching,
	};
};

export default connect(mapStateToProps)(App);
