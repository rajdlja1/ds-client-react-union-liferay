import PropTypes from 'prop-types';
import { LIFERAY_HOST_PUBLIC_SPACE, TRAINSET_OVERVIEW_PAGE } from '../../../constants';
import renderVehicle from '../../../components/Vehicle';

const React = require('react');

class Trainset extends React.Component {
	static SHOW_TRAINSETS_LABEL = 'Zobrazit spoje';
	static EDIT_TRAINSET_LABEL = 'Editovat soupravu';
	static DELETE_TRAINSET_LABEL = 'Smazat soupravu';

	constructor(props) {
		super(props);
		this.state = { display: true };
	}

	render() {
		const trainsetId = this.props.trainset.id;
		const cars = [];
		this.props.trainset.vehicles.forEach(function(car) {
			cars.push(renderVehicle(car, trainsetId));
		});

		const link = `${LIFERAY_HOST_PUBLIC_SPACE}/${TRAINSET_OVERVIEW_PAGE}?id=${trainsetId}`;
		if (this.state.display === false) return null;
		else
			return (
				<tr key={this.props.trainset.id}>
					<td>
						<a href={link}>{this.props.trainset.name}</a>
					</td>
					<td>{cars.map(c => c).reduce((prev, curr) => [prev, ' ', curr])}</td>
					<td>
						<div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
							<div className="btn-group mr-2" role="group" aria-label="First group">
								<button className="btn btn-info">{Trainset.EDIT_TRAINSET_LABEL}</button>
							</div>
							<div className="btn-group mr-2" role="group" aria-label="First group">
								<button className="btn btn-info">{Trainset.SHOW_TRAINSETS_LABEL}</button>
							</div>
						</div>
					</td>
				</tr>
			);
	}
}

Trainset.propTypes = {
	showRoutes: PropTypes.func,
	trainset: PropTypes.shape({
		trains: PropTypes.arrayOf(
			PropTypes.shape({
				name: PropTypes.string,
				description: PropTypes.string,
			})
		),
		vehicles: PropTypes.arrayOf(
			PropTypes.shape({
				name: PropTypes.string,
				description: PropTypes.string,
			})
		),
		name: PropTypes.string,
		id: PropTypes.number,
	}),
};

export default Trainset;
