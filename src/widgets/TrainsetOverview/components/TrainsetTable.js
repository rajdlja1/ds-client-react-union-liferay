/* eslint-disable react/prefer-stateless-function */
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Trainset from './Trainset';

const React = require('react');

class TrainsetTable extends React.Component {
	render() {
		const rows = [];
		Array.prototype.forEach.call(this.props.trainsets, trainset => {
			rows.push(<Trainset key={trainset.id} trainset={trainset} />);
		});

		return (
			<table className="table table-striped">
				<thead>
					<tr>
						<th>
							<FormattedMessage id="label.trainSets" />
						</th>
						<th>
							<FormattedMessage id="label.cars" />
						</th>
						<th>
							<FormattedMessage id="label.actions" />
						</th>
					</tr>
				</thead>
				<tbody>{rows}</tbody>
			</table>
		);
	}
}

TrainsetTable.propTypes = {
	showRoutes: PropTypes.func,
	trainsets: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string,
			description: PropTypes.string,
		})
	),
};

export default TrainsetTable;
