import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { IntlProvider, addLocaleData } from 'react-intl';
import cs from 'react-intl/locale-data/cs';
import sk from 'react-intl/locale-data/sk';
import { createLogger } from 'redux-logger';
import reducer from './reducers';
import App from './containers/App';

import messages_cs from '../../translations/cs.json';
import messages_en from '../../translations/en.json';

const messages = {
	cs: messages_cs,
	en: messages_en,
};

const language = navigator.language.split(/[-_]/)[0]; // language without region code

const middleware = [thunk];
if (process.env.NODE_ENV !== 'production') {
	middleware.push(createLogger());
}

const store = createStore(reducer, applyMiddleware(...middleware));

addLocaleData([...cs, ...sk]);

const Root = () => (
	<Provider store={store}>
		<IntlProvider locale={language} messages={messages[language]}>
			<App />
		</IntlProvider>
	</Provider>
);

export default Root;
