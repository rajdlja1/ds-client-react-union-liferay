import { combineReducers } from 'redux';
import {
	SELECT_DATE,
	CHANGE_CHECKBOX_VALUE,
	INVALIDATE_FORM,
	REQUEST_POSTS,
	RECEIVE_POSTS,
} from '../actions';

const selectedDate = (state = new Date(), action) => {
	switch (action.type) {
		case SELECT_DATE:
			return action.date;
		default:
			return state;
	}
};

const changedCheckboxValue = (state = 'NOTHING_SELECTED', action) => {
	switch (action.type) {
		case CHANGE_CHECKBOX_VALUE:
			return action.checkboxValue;
		default:
			return state;
	}
};

const trainsetsMan = (
	state = {
		isFetching: false,
		didInvalidate: false,
		items: [],
	},
	action
) => {
	switch (action.type) {
		case INVALIDATE_FORM:
			return {
				...state,
				didInvalidate: true,
			};
		case REQUEST_POSTS:
			return {
				...state,
				isFetching: true,
				didInvalidate: false,
			};
		case RECEIVE_POSTS:
			return {
				...state,
				isFetching: false,
				didInvalidate: false,
				items: action.posts,
				lastUpdated: action.receivedAt,
			};
		default:
			return state;
	}
};

const filteredTrainsets = (state = {}, action) => {
	switch (action.type) {
		case INVALIDATE_FORM:
		case RECEIVE_POSTS:
		case REQUEST_POSTS:
			return {
				...state,
				trainsets: trainsetsMan(state.trainsets, action),
			};
		default:
			return state;
	}
};

const rootReducer = combineReducers({
	filteredTrainsets,
	selectedDate,
	changedCheckboxValue,
});

export default rootReducer;
