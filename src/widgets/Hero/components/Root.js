import React from 'react';

import logo from './logo.png';

const Root = () => (
	<div className="container">
		<img className="img-fluid" src={logo} />
		<nav className="navbar navbar-default">
			<div className="container-fluid">
				<div className="navbar-header" />
				<ul className="nav navbar-nav">
					<li className="active">
						<a href="#">Home</a>
					</li>
					<li>
						<a href="#">Soupravy</a>
					</li>
					<li>
						<a href="#">Spoje</a>
					</li>
					<li>
						<a href="#">Nastavení</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
);

export default Root;
