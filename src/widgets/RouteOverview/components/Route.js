import React from 'react';
import PropTypes from 'prop-types';
import { FormattedTime, injectIntl, intlShape } from 'react-intl';
import { connect } from 'react-redux';
import { showTrainsChanged} from '../../RouteOverview/actions';
import { LIFERAY_HOST_PUBLIC_SPACE, ROUTE_OVERVIEW_PAGE } from '../../../constants';
import Train from '../../../components/Train';
import renderRide from '../../../components/Ride';

function renderCar(car, setId) {
	return <span key={`${setId}-${car.id}-${car.description}`}>{car.name}</span>;
}

function renderLocomotive(train, setId) {
	return <span key={`${setId}-${train.id}`}>{train.name}</span>;
}

class Route extends React.Component {
	handleShowTrainsChanged = () => {
		this.props.dispatch(showTrainsChanged(this.props.route.id));
	};

	render() {
		const { intl } = this.props;

		const routeId = this.props.route.id;
		const locomotives = [];
		if (this.props.route.rides != null && this.props.route.rides.length > 0) {
			this.props.route.rides[0].locomotives.forEach(function(locomotive) {
				locomotives.push(renderLocomotive(locomotive, routeId));
			});
		}

		const cars = [];

		if (this.props.route.rides != null && this.props.route.rides.length > 0) {
			this.props.route.rides[0].cars.forEach(function(car) {
				cars.push(renderCar(car, routeId));
			});
		}

		const trains = [];

		if (this.props.route.trains != null && this.props.route.trains.length > 0) {
			this.props.route.trains.forEach(function(train) {
				trains.push(<Train key={train.id} train={train} />);
			});
		}
		const rides = [];

		if (this.props.route.rides != null && this.props.route.rides.length > 0) {
			this.props.route.rides.forEach(function(ride) {
				rides.push(renderRide(`${routeId}-${ride.id}`, ride));
			});
		}

		const link = `${LIFERAY_HOST_PUBLIC_SPACE}/${ROUTE_OVERVIEW_PAGE}?id=${this.props.route.id}`;
		return (
			<tbody>
			<tr key={this.props.route.id} className="trainRow">
				<td>
					<a href={link}>
						<span className="name">{this.props.route.name}</span>
					</a>
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>{this.props.route.from.location}</td>
				<td>
					<FormattedTime value={this.props.route.departureTime} />
				</td>
				<td>{this.props.route.to.location}</td>
				<td>
					<FormattedTime value={this.props.route.arrivalTime} />
				</td>
				<td>
					<div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
						<div className="btn-group mr-2" role="group" aria-label="First group">
							<button
								className="btn btn-info"
								onClick={() => this.handleShowTrainsChanged(this.props.route.id)}
							>
								{this.props.route.showTrains !== false
									? intl.formatMessage({ id: 'label.button.hideTrains' })
									: intl.formatMessage({ id: 'label.button.showTrains' })}
							</button>
						</div>
						<div className="btn-group mr-2" role="group" aria-label="First group" />
					</div>
				</td>
			</tr>
			{this.props.route.showTrains !== false && trains}
			</tbody>
		);
	}
}

Route.propTypes = {
	dispatch: PropTypes.func.isRequired,
	intl: intlShape.isRequired,
	route: PropTypes.shape({
		from: PropTypes.shape({
			location: PropTypes.string,
		}),
		name: PropTypes.string,
		to: PropTypes.shape({
			location: PropTypes.string,
		}),
		arrivalTime: PropTypes.string,
		departureTime: PropTypes.string,
		trains: PropTypes.arrayOf(
			PropTypes.shape({
				description: PropTypes.string,
				arrivalTime: PropTypes.string,
				departureTime: PropTypes.string,
			})
		),
		trainset: PropTypes.shape({
			description: PropTypes.number,
		}),
		cars: PropTypes.arrayOf(
			PropTypes.shape({
				name: PropTypes.string,
				description: PropTypes.string,
			})
		),
		id: PropTypes.number,
		showRouteSections: PropTypes.bool,
		rides: PropTypes.array,
		driver: PropTypes.shape({
			id: PropTypes.number,
			firstname: PropTypes.string,
			lastname: PropTypes.string,
			phoneNumber: PropTypes.string,
		}),
		route: PropTypes.shape({
			from: PropTypes.shape({
				location: PropTypes.string,
			}),
			name: PropTypes.string,
			to: PropTypes.shape({
				location: PropTypes.string,
			}),
			departureTime: PropTypes.string,
			arrivalTime: PropTypes.string,
		}),
	}),
	trainset: PropTypes.shape({
		id: PropTypes.number,
	}),
};

const mapStateToProps = () => {
	return {};
};

export default connect(mapStateToProps)(injectIntl(Route));
