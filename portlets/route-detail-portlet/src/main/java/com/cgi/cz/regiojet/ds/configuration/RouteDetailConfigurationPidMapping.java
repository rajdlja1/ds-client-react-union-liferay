package com.cgi.cz.regiojet.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import com.cgi.cz.regiojet.ds.constants.RouteDetailPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class RouteDetailConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return RouteDetailConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return RouteDetailPortletKeys.ROUTE_DETAIL;
    }
}
