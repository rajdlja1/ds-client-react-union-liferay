package com.cgi.cz.regiojet.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class RouteDetailPortletKeys {

	public static final String ROUTE_DETAIL = "com_cgi_cz_regiojet_ds_portlet_Route_Detail";
	public static final String CONFIGURATION = "com.cgi.cz.regiojet.ds.configuration.RouteDetailConfiguration";

}