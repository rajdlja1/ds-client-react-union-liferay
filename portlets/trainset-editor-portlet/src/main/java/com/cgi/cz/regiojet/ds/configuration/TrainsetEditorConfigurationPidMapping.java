package com.cgi.cz.regiojet.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import com.cgi.cz.regiojet.ds.constants.TrainsetEditorPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class TrainsetEditorConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return TrainsetEditorConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return TrainsetEditorPortletKeys.TRAINSET_EDITOR;
    }
}
