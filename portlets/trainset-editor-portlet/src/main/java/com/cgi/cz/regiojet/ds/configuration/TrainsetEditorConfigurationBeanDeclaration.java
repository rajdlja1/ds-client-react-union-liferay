package com.cgi.cz.regiojet.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationBeanDeclaration;
import org.osgi.service.component.annotations.Component;

/**
 * Registers the configuration class {@link TrainsetEditorConfiguration}. It enables the system to keep track of any
 * configuration changes as they happen.
 *
 * @author Jan Rajdl
 */
@Component
public class TrainsetEditorConfigurationBeanDeclaration implements ConfigurationBeanDeclaration {

    /**
     * Returns configuration class.
     *
     * @return configuration class
     */
    @Override
    public Class<?> getConfigurationBeanClass() {
        return TrainsetEditorConfiguration.class;
    }
}
