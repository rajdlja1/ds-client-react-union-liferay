package com.cgi.cz.regiojet.ds.configuration;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import static com.cgi.cz.regiojet.ds.constants.TrainsetEditorConstants.*;

/**
 * Util class containing useful methods for the work with the portlet configuration.
 *
 * @author Jan Rajdl
 */
public class TrainsetEditorConfigurationUtil {

    /**
     * Adds configuration properties to a render request as the attributes.
     *
     * @param request render request
     * @throws ConfigurationException if the portlet configuration can't be retrieved
     */
    public static void addConfigurationContext(RenderRequest request) throws ConfigurationException {
        final ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        if (themeDisplay != null) {
            TrainsetEditorConfiguration trainsetEditorConfiguration = getTrainsetEditorConfiguration(themeDisplay);

            request.setAttribute(ATTR_HEADING, trainsetEditorConfiguration.heading());
            request.setAttribute(ATTR_CONTENT, trainsetEditorConfiguration.content());
        }
    }

    /**
     * Adds configuration properties to a servlet request as the attributes.
     *
     * @param request servlet request
     * @throws ConfigurationException if the portlet configuration can't be retrieved
     */
    public static void addConfigurationContext(HttpServletRequest request) throws ConfigurationException {
        final ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        if (themeDisplay != null) {
            TrainsetEditorConfiguration trainsetEditorConfiguration = getTrainsetEditorConfiguration(themeDisplay);

            request.setAttribute(ATTR_HEADING, trainsetEditorConfiguration.heading());
            request.setAttribute(ATTR_CONTENT, trainsetEditorConfiguration.content());
        }
    }

    private static TrainsetEditorConfiguration getTrainsetEditorConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
        final PortletDisplay portletDisplay = themeDisplay.getPortletDisplay();
        return portletDisplay.getPortletInstanceConfiguration(TrainsetEditorConfiguration.class);
    }

}
