package com.cgi.cz.regiojet.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class TrainsetEditorPortletKeys {

	public static final String TRAINSET_EDITOR = "com_cgi_cz_regiojet_ds_portlet_Trainset_Editor";
	public static final String CONFIGURATION = "com.cgi.cz.regiojet.ds.configuration.TrainsetEditorConfiguration";

}