package com.cgi.cz.regiojet.ds.configuration;

import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.cgi.cz.regiojet.ds.constants.TrainsetEditorPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;

/**
 * The configuration action class enables Configuration item in the portlet's menu with gear icon.
 * Configuration.jsp is used to render the UI.
 *
 * @author Jan Rajdl
 */
@Component(
    configurationPid = TrainsetEditorPortletKeys.CONFIGURATION,
    configurationPolicy = ConfigurationPolicy.OPTIONAL,
    immediate = true,
    property = {
        "javax.portlet.name=" + TrainsetEditorPortletKeys.TRAINSET_EDITOR,
    },
    service = ConfigurationAction.class
)
public class TrainsetEditorConfigurationAction extends DefaultConfigurationAction {

    @Override
    public void include(PortletConfig portletConfig, HttpServletRequest request, HttpServletResponse response) throws Exception {
        TrainsetEditorConfigurationUtil.addConfigurationContext(request);

        super.include(portletConfig, request, response);
    }

}
