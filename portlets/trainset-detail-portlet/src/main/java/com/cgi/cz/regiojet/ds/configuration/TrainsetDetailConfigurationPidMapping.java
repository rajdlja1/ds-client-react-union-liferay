package com.cgi.cz.regiojet.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import com.cgi.cz.regiojet.ds.constants.TrainsetDetailPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class TrainsetDetailConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return TrainsetDetailConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return TrainsetDetailPortletKeys.TRAINSET_DETAIL;
    }
}
