package com.cgi.cz.regiojet.ds.configuration;

import aQute.bnd.annotation.metatype.Meta;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.cgi.cz.regiojet.ds.constants.TrainsetDetailPortletKeys;

/**
 * Configuration class of the Trainset Detail portlet.
 *
 * @author Jan Rajdl
 */
@ExtendedObjectClassDefinition(
        category = "foundation",
        scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
)
@Meta.OCD(id = TrainsetDetailPortletKeys.CONFIGURATION)
public interface TrainsetDetailConfiguration {

    @Meta.AD(
            required = true
    )
    String heading();

    @Meta.AD(
            required = true
    )
    String content();

}
