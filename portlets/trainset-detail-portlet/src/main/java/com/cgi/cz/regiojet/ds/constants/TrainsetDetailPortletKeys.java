package com.cgi.cz.regiojet.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class TrainsetDetailPortletKeys {

	public static final String TRAINSET_DETAIL = "com_cgi_cz_regiojet_ds_portlet_Trainset_Detail";
	public static final String CONFIGURATION = "com.cgi.cz.regiojet.ds.configuration.TrainsetDetailConfiguration";

}