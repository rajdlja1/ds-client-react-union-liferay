<%--
Renders React widgets.
--%>
<%@ include file="init.jsp" %>

<%--suppress JSUnresolvedVariable, JSUnresolvedFunction --%>
<script type="text/javascript">
	Liferay.Loader.require("entry-module");
</script>

<div id="${ns}hero"></div>
<script data-union-widget="hero" data-union-container="${ns}hero" type="application/json"></script>

<h2>Detail spoje ${id}</h2>

<div id="${ns}content"></div>
<script data-union-widget="content" data-union-container="${ns}content" type="application/json">
    {
        "textation": {
            "heading": "${heading}",
            "content": "${content}"
        },
        "id": "${id}"
    }
</script>

<div id="trainsetDetail"></div>
<script data-union-widget="trainsetDetail" data-union-container="trainsetDetail" type="application/json"></script>