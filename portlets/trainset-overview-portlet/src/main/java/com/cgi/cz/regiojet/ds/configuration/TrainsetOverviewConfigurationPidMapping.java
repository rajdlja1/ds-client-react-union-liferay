package com.cgi.cz.regiojet.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import com.cgi.cz.regiojet.ds.constants.TrainsetOverviewPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class TrainsetOverviewConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return TrainsetOverviewConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return TrainsetOverviewPortletKeys.TRAINSET_OVERVIEW;
    }
}
