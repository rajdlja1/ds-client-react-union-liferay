package com.cgi.cz.regiojet.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class TrainsetOverviewPortletKeys {

	public static final String TRAINSET_OVERVIEW = "com_cgi_cz_regiojet_ds_portlet_Trainset_Overview";
	public static final String CONFIGURATION = "com.cgi.cz.regiojet.ds.configuration.TrainsetOverviewConfiguration";

}