package com.cgi.cz.regiojet.ds.configuration;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import static com.cgi.cz.regiojet.ds.constants.TrainsetOverviewConstants.*;

/**
 * Util class containing useful methods for the work with the portlet configuration.
 *
 * @author Jan Rajdl
 */
public class TrainsetOverviewConfigurationUtil {

    /**
     * Adds configuration properties to a render request as the attributes.
     *
     * @param request render request
     * @throws ConfigurationException if the portlet configuration can't be retrieved
     */
    public static void addConfigurationContext(RenderRequest request) throws ConfigurationException {
        final ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        if (themeDisplay != null) {
            TrainsetOverviewConfiguration trainsetOverviewConfiguration = getTrainsetOverviewConfiguration(themeDisplay);

            request.setAttribute(ATTR_HEADING, trainsetOverviewConfiguration.heading());
            request.setAttribute(ATTR_CONTENT, trainsetOverviewConfiguration.content());
        }
    }

    /**
     * Adds configuration properties to a servlet request as the attributes.
     *
     * @param request servlet request
     * @throws ConfigurationException if the portlet configuration can't be retrieved
     */
    public static void addConfigurationContext(HttpServletRequest request) throws ConfigurationException {
        final ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        if (themeDisplay != null) {
            TrainsetOverviewConfiguration trainsetOverviewConfiguration = getTrainsetOverviewConfiguration(themeDisplay);

            request.setAttribute(ATTR_HEADING, trainsetOverviewConfiguration.heading());
            request.setAttribute(ATTR_CONTENT, trainsetOverviewConfiguration.content());
        }
    }

    private static TrainsetOverviewConfiguration getTrainsetOverviewConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
        final PortletDisplay portletDisplay = themeDisplay.getPortletDisplay();
        return portletDisplay.getPortletInstanceConfiguration(TrainsetOverviewConfiguration.class);
    }

}
