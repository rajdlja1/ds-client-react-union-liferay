package com.cgi.cz.regiojet.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import com.cgi.cz.regiojet.ds.constants.RouteOverviewPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class RouteOverviewConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return RouteOverviewConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return RouteOverviewPortletKeys.ROUTE_OVERVIEW;
    }
}
