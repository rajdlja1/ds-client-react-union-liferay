package com.cgi.cz.regiojet.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class RouteOverviewPortletKeys {

	public static final String ROUTE_OVERVIEW = "com_cgi_cz_regiojet_ds_portlet_Route_Overview";
	public static final String CONFIGURATION = "com.cgi.cz.regiojet.ds.configuration.RouteOverviewConfiguration";

}