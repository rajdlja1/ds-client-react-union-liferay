package com.cgi.cz.regiojet.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import com.cgi.cz.regiojet.ds.constants.TrainOverviewPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class TrainOverviewConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return TrainOverviewConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return TrainOverviewPortletKeys.TRAIN_OVERVIEW;
    }
}
