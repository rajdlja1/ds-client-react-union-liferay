package com.cgi.cz.regiojet.ds.portlet;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;

import com.cgi.cz.regiojet.ds.configuration.TrainOverviewConfigurationUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.cgi.cz.regiojet.ds.configuration.TrainOverviewConfigurationUtil;
import com.cgi.cz.regiojet.ds.constants.TrainOverviewPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * Controller of Train Overview Portlet, it renders view.jsp with the React widgets.
 *
 * @author Jan Rajdl
 */
@Component(
	configurationPid = TrainOverviewPortletKeys.CONFIGURATION,
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + TrainOverviewPortletKeys.TRAIN_OVERVIEW,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class TrainOverviewPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			TrainOverviewConfigurationUtil.addConfigurationContext(renderRequest);
		} catch (ConfigurationException e) {
			throw new PortletException("Configuration error", e);
		}

		super.render(renderRequest, renderResponse);
	}

}