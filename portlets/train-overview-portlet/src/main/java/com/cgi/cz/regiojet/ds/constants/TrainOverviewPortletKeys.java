package com.cgi.cz.regiojet.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class TrainOverviewPortletKeys {

	public static final String TRAIN_OVERVIEW = "com_cgi_cz_regiojet_ds_portlet_Train_Overview";
	public static final String CONFIGURATION = "com.cgi.cz.regiojet.ds.configuration.TrainOverviewConfiguration";

}