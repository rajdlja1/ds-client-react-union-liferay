package com.cgi.cz.regiojet.ds.constants;

/**
 * Train Overview widget's constants.
 *
 * @author Jan Rajdl
 */
public class TrainOverviewConstants {

    public static final String ATTR_HEADING = "heading";
    public static final String ATTR_CONTENT = "content";

    public static final String PARAM_HEADING = "heading";
    public static final String PARAM_CONTENT = "content";

}
